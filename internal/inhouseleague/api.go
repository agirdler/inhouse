package inhouseleague

import (
	"math"
	"sort"
	"strconv"

	"github.com/beltsmith/golio/model"
	"gitlab.com/agirdler/inhouse/internal/inhouseleague/models"
)

const SOLO_QUEUE = 420 // nice
const SOLO_LEAGUE = "RANKED_SOLO_5x5"

func (s *Service) summonerSpellByKey(keyInt int) *models.SummonerSpell {
	key := strconv.Itoa(keyInt)
	summonerSpell := &models.SummonerSpell{Key: key}

	// Sync on first failure
	if s.DB.First(summonerSpell, summonerSpell).RecordNotFound() {
		s.SyncSummonerSpells()
	}

	// Either load from data or return result with just key
	// will fail to load images but fine otherwise
	s.DB.First(summonerSpell, summonerSpell)

	return summonerSpell
}

func participantMapFor(game *model.Match) map[string]model.Participant {
	idents := game.ParticipantIdentities
	participantMap := map[string]model.Participant{}

	participants := game.Participants
	sort.Slice(participants, func(i, j int) bool {
		return participants[i].ParticipantID <= participants[j].ParticipantID
	})

	byParticipantId := func(participantID int) func(int) bool {
		return func(i int) bool {
			return participants[i].ParticipantID >= participantID
		}
	}

	for _, ident := range idents {
		participantIdx := sort.Search(len(participants), byParticipantId(ident.ParticipantID))
		participant := participants[participantIdx]
		participantMap[ident.Player.SummonerID] = participant
	}

	return participantMap
}

func participantFor(summonerID string, game *model.Match) model.Participant {
	return participantMapFor(game)[summonerID]
}

func teamFor(participant model.Participant, game *model.Match) model.TeamStats {
	teams := game.Teams
	sort.Slice(teams, func(i, j int) bool {
		return teams[i].TeamID <= teams[j].TeamID
	})
	teamIdx := sort.Search(len(teams), func(i int) bool {
		return teams[i].TeamID >= participant.TeamID
	})

	return teams[teamIdx]
}

func (c *Client) resultFor(game *model.Match, participant model.Participant) string {
	team := teamFor(participant, game)
	var result string

	if game.GameDuration < 300 {
		result = "Remake"
	} else if team.Win == "Win" {
		result = "Win"
	} else {
		result = "Loss"
	}

	return result
}

func (c *Client) getRankedLeagueByPlayer(player *models.Player) *model.LeagueEntry {
	summoner := player.Summoner
	matchHistory := player.Matches
	leagues, err := c.GetLeaguesBySummoner(summoner.RemoteID)

	// Handle case where summoner is not ranked yet
	if err != nil || len(leagues) == 0 {
		wins, losses := 0, 0
		for _, match := range matchHistory {
			if match.Win == "Win" {
				wins = wins + 1
			} else {
				losses = losses + 1
			}
		}

		return &model.LeagueEntry{
			LeaguePoints: 0,
			Tier:         "Qualifiers",
			Rank:         "",
			Wins:         wins,
			Losses:       losses,
		}
	}

	sort.Slice(leagues, func(i, j int) bool {
		return leagues[i].Queue <= leagues[j].Queue
	})
	leagueIdx := sort.Search(len(leagues), func(i int) bool {
		return leagues[i].Queue >= SOLO_LEAGUE
	})

	return leagues[leagueIdx]
}

func (s *Service) SyncSummonerSpells() {
	summonerSpells, err := s.Api.GetSummonerSpells()
	if err != nil {
		panic("Failed to fetch summoner spell data")
	}

	for _, summonerSpellData := range summonerSpells {
		summonerSpell := &models.SummonerSpell{
			Key:      summonerSpellData.Key,
			Name:     summonerSpellData.Name,
			RemoteID: summonerSpellData.ID,
		}
		s.DB.FindOrCreateBy(summonerSpell)
	}
}

func (c *Client) CreateSummoner(summonerName string) *models.Summoner {
	summonerData, _ := c.GetSummonerByName(summonerName)
	return &models.Summoner{
		RemoteID:  summonerData.ID,
		Name:      summonerData.Name,
		Level:     summonerData.SummonerLevel,
		AccountID: summonerData.AccountID,
	}
}

func (c *Client) CreatePlayer(summonerName string) *models.Player {
	summoner := c.CreateSummoner(summonerName)

	return &models.Player{
		Summoner: summoner,
	}
}

func (c *Client) CreateTournament(name string, summonerNames []string) *models.Tournament {
	var players []*models.Player
	for _, summonerName := range summonerNames {
		players = append(players, c.CreatePlayer(summonerName))
	}

	return &models.Tournament{
		Name:    name,
		Players: players,
		Active:  true,
	}
}

func (s *Service) SyncPlayerMatchHistory(player *models.Player) {
	summoner := player.Summoner
	matchStream := s.Api.GetQueueMatchesByAccountStream(summoner.AccountID, SOLO_QUEUE)

	for {
		msg := <-matchStream
		if msg.Error != nil {
			break
		}

		matchData := msg.MatchReference
		championKey := strconv.Itoa(matchData.Champion)
		game, _ := s.Api.GetMatch(matchData.GameID)

		participant := participantFor(summoner.RemoteID, game)

		championName := s.DB.ChampionNameByKey(championKey)

		result := s.Api.resultFor(game, participant)

		match := &models.Match{
			PlayerID: player.ID,
			RemoteID: matchData.GameID,
			Queue:    matchData.Queue,
			Win:      result,
			Champion: championName,
			Spell1:   s.summonerSpellByKey(participant.Spell1ID).RemoteID,
			Spell2:   s.summonerSpellByKey(participant.Spell2ID).RemoteID,
		}

		if s.DB.Debug().Where(match).First(match).RecordNotFound() != true {
			break
		}
		s.DB.Debug().Create(match)
	}

	player.Matches = s.DB.MatchesByPlayer(player)
}

func (s *Service) SyncPlayerRank(player *models.Player) {
	leagueEntry := s.Api.getRankedLeagueByPlayer(player)

	winRateRaw := float64(leagueEntry.Wins) / float64(leagueEntry.Wins+leagueEntry.Losses)
	winRate := int(math.Round(winRateRaw * 100))

	rank := s.DB.RankByPlayer(player)

	// TODO check if just setting the fields makes Update work properly
	newRank := &models.Rank{
		PlayerID:     player.ID,
		Tier:         leagueEntry.Tier,
		Division:     leagueEntry.Rank,
		LeaguePoints: leagueEntry.LeaguePoints,
		WinRate:      winRate,
		Wins:         leagueEntry.Wins,
		Losses:       leagueEntry.Losses,
	}

	if !rank.Equal(*newRank) {
		s.DB.Debug().Model(rank).Where(&models.Rank{PlayerID: rank.PlayerID}).Update(newRank)
	}

	player.Rank = newRank
}

func (s *Service) SyncChampions() {
	champions, err := s.Api.GetChampions()

	if err != nil {
		return
	}

	for _, championData := range champions {
		champion := &models.Champion{
			Name: championData.Name,
			Key:  championData.Key,
		}
		s.DB.FindOrCreateBy(champion)
	}
}

func (s *Service) SyncPlayerSummoner(player *models.Player) {
	summoner := s.DB.SummonerByPlayer(player)
	summonerData := s.Api.CreateSummoner(summoner.Name)

	s.DB.Where(summoner).Assign(summonerData).FirstOrCreate(summoner)

	player.Summoner = summoner
}

func (s *Service) updatePlayer(player *models.Player) {
	s.SyncPlayerSummoner(player)
	s.SyncPlayerMatchHistory(player)
	s.SyncPlayerRank(player)
}

func (s *Service) SyncTournament(tournament *models.Tournament) {
	for _, player := range tournament.Players {
		s.updatePlayer(player)
	}
}

func (s *Service) GetTournament(tournamentID uint) (*models.Tournament, error) {
	tournament, err := s.DB.TournamentByID(tournamentID)

	if err != nil {
		return nil, err
	}

	return tournament, nil
}

func (s *Service) GetTournamentByName(tournamentID uint) (*models.Tournament, error) {
	tournament, err := s.DB.TournamentByID(tournamentID)

	if err != nil {
		return nil, err
	}

	return tournament, nil
}

func (s *Service) GetActiveTournament() (*models.Tournament, error) {
	tournament, err := s.DB.ActiveTournament()

	if err != nil {
		return nil, err
	}

	return tournament, nil
}
