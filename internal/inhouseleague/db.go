package inhouseleague

import (
	"encoding/json"
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"gitlab.com/agirdler/inhouse/internal/inhouseleague/models"
	"os"
)

// Methods

func (db *DB) ActiveTournament() (*models.Tournament, error) {
	tournament := &models.Tournament{Active: true}

	err := db.PreloadPlayers().First(tournament, tournament).Error

	if gorm.IsRecordNotFoundError(err) {
		return nil, err
	}

	return tournament, nil
}

func (db *DB) TournamentByID(tournamentID uint) (*models.Tournament, error) {
	tournament := &models.Tournament{}

	err := db.PreloadPlayers().First(tournament, "id = ?", tournamentID).Error

	if gorm.IsRecordNotFoundError(err) {
		return nil, err
	}

	return tournament, nil
}

func (db *DB) ChampionNameByKey(championKey string) string {
	champion := &models.Champion{
		Key:  championKey,
		Name: "",
	}
	db.Where(champion).First(champion)
	return champion.Name
}

func (db *DB) SummonerByPlayer(player *models.Player) *models.Summoner {
	summoner := &models.Summoner{
		PlayerID: player.ID,
	}
	db.Where(summoner).First(summoner)

	return summoner
}

func (db *DB) RankByPlayer(player *models.Player) *models.Rank {
	rank := &models.Rank{
		PlayerID: player.ID,
	}
	db.Where(rank).First(rank)

	return rank
}

func (db *DB) MatchesByPlayer(player *models.Player) []*models.Match {
	match := &models.Match{
		PlayerID: player.ID,
	}
	matchHistory := []models.Match{}

	db.Where(match).Order("remote_id desc").Find(&matchHistory)

	matches := make([]*models.Match, len(matchHistory))

	for idx, match := range matchHistory {
		matches[idx] = &match
	}

	return matches
}

func (db *DB) FindOrCreateBy(record interface{}) {
	db.Where(record).FirstOrCreate(record)
}

func preloadFullMatchesOrdered(db *gorm.DB) *gorm.DB {
	return db.Where("matches.win <> ?", "Remake").Order("matches.remote_id DESC")
}

func (db *DB) PreloadPlayers() *DB {
	return &DB{
		db.Preload("Players").Preload("Players.Summoner").Preload("Players.Rank").Preload("Players.Matches", preloadFullMatchesOrdered),
	}
}

// Setup

type DB struct {
	*gorm.DB
}

func migrateDB(db *DB) {
	db.AutoMigrate(&models.Tournament{})
	db.AutoMigrate(&models.Player{})
	db.AutoMigrate(&models.Rank{})
	db.AutoMigrate(&models.Summoner{})
	db.AutoMigrate(&models.Match{})
	db.AutoMigrate(&models.Champion{}, &models.SummonerSpell{})
}

type DBCreds struct {
	Host        string `json:"host"`
	User        string `json:"username"`
	Password    string `json:"password"`
	DBName      string `json:"dbname"`
	Environment string
}

func (creds *DBCreds) ToConfig() string {
	config := fmt.Sprintf("host=%s user=%s dbname=inhouse password=%s", creds.Host, creds.User, creds.Password)
	if creds.Environment == "development" {
		config = config + " sslmode=disable"
	}
	return config
}

func getProdDBCreds() *DBCreds {
	dbCreds := &DBCreds{
		Environment: "production",
	}
	json.Unmarshal([]byte(os.Getenv("DB_CREDS")), dbCreds)
	return dbCreds
}

func getLocalDBCreds() *DBCreds {
	return &DBCreds{
		Host:        os.Getenv("DB_HOSTNAME"),
		User:        os.Getenv("DB_USERNAME"),
		Password:    os.Getenv("DB_PASSWORD"),
		DBName:      "inhouse",
		Environment: "development",
	}
}

func getDBCreds(environment string) *DBCreds {
	if environment == "development" {
		return getLocalDBCreds()
	}
	return getProdDBCreds()
}

func SetupDB(environment string) *DB {
	dbCreds := getDBCreds(environment)

	dbConnection, err := gorm.Open("postgres", dbCreds.ToConfig())
	if err != nil {
		fmt.Println(err)
		fmt.Printf("%+v", dbCreds)
		panic("Could not connect to DB")
	}
	db := &DB{dbConnection}
	migrateDB(db)

	return db
}
