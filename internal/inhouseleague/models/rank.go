package models

import (
	"fmt"
	"github.com/jinzhu/gorm"
)

type Rank struct {
	gorm.Model
	PlayerID     uint
	Tier         string `json:"tier"`
	Division     string `json:"division"`
	LeaguePoints int    `json:"leaguePoints"`
	WinRate      int    `json:"winRate"`
	Wins         int    `json:"wins"`
	Losses       int    `json:"losses"`
}

func (l Rank) Equal(r Rank) bool {
	if l.PlayerID != r.PlayerID {
		fmt.Println("pid")
		return false
	}

	if l.Tier != r.Tier {
		fmt.Println("tier")
		return false
	}

	if l.Division != r.Division {
		fmt.Println("division")
		return false
	}

	if l.LeaguePoints != r.LeaguePoints {
		fmt.Println("lp")
		return false
	}

	if l.Wins != r.Wins {
		return false
	}

	if l.Losses != r.Losses {
		return false
	}

	return true
}
