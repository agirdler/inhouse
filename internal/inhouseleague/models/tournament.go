package models

import (
	"github.com/jinzhu/gorm"
)

type Tournament struct {
	gorm.Model
	Name    string    `gorn:"unique;not null"`
	Players []*Player `json:"players"`
	Active  bool      `json:"active"`
}
