package models

import (
	"github.com/jinzhu/gorm"
)

type SummonerSpell struct {
	gorm.Model
	Key      string `json:"key"`
	Name     string `json:"name"`
	RemoteID string `json:"remote_id"`
}
