package models

import (
	"github.com/jinzhu/gorm"
)

type Summoner struct {
	gorm.Model
	PlayerID  uint   `json:"player_id"`
	RemoteID  string `json:"remote_id" gorm:"unique;not null"`
	Name      string `json:"name" gorm:"unique;not null"`
	Level     int    `json:"level"`
	AccountID string `json:"accountID" gorm:"unique;not null"`
}
