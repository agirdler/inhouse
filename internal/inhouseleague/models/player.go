package models

import (
	"github.com/jinzhu/gorm"
)

type Player struct {
	gorm.Model
	TournamentID uint      `json:"tournament_id"`
	Summoner     *Summoner `json:"summoner"`
	Rank         *Rank     `json:"rank"`
	Matches      []*Match  `json:"match_list"`
}
