package models

type Champion struct {
	Name string `json:"name" gorm:"unique;not null"`
	Key  string
}
