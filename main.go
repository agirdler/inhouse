package main

import (
	"net/http"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"

	"gitlab.com/agirdler/inhouse/internal/inhouseleague"
	"os"
	"strconv"
)

func handleDashboard(c *gin.Context) {
	c.HTML(http.StatusOK, "dashboard.tmpl", gin.H{
		"assetHost": "//assets.inhouse.gg",
	})
}

func setupRouter() *gin.Engine {
	// Disable Console Color
	// gin.DisableConsoleColor()
	r := gin.Default()

	service := inhouseleague.SetupService(os.Getenv("INHOUSE_ENV"))

	r.Use(cors.Default())

	r.LoadHTMLGlob("web/templates/*")

	// Ping test
	r.GET("/ping", func(c *gin.Context) {
		c.String(http.StatusOK, "pong")
	})

	r.GET("/", handleDashboard)
	r.GET("/dashboard", handleDashboard)

	// TODO refactor
	r.GET("/active_tournament", func(c *gin.Context) {
		tournament, err := service.GetActiveTournament()

		if err != nil {
			c.JSON(http.StatusNotFound, gin.H{"error": "Not Found"})
		}

		c.JSON(http.StatusOK, tournament)
	})

	r.GET("/active_tournament/update", func(c *gin.Context) {
		tournament, err := service.GetActiveTournament()

		if err != nil {
			c.JSON(http.StatusNotFound, gin.H{"error": "Not Found"})
		}

		service.SyncTournament(tournament)

		c.JSON(http.StatusOK, tournament)
	})

	r.GET("/active_tournament/update_async", func(c *gin.Context) {
		tournament, err := service.GetActiveTournament()

		if err != nil {
			c.JSON(http.StatusNotFound, gin.H{"error": "Not Found"})
		}

		go service.SyncTournament(tournament)

		c.JSON(http.StatusOK, tournament)
	})

	r.GET("/tournament/:id", func(c *gin.Context) {
		tournamentID, err := strconv.Atoi(c.Params.ByName("id"))
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{})
		}

		tournament, err := service.GetTournament(uint(tournamentID))

		if err != nil {
			c.JSON(http.StatusNotFound, gin.H{})
		}

		c.JSON(http.StatusOK, tournament)
	})

	r.GET("/tournament/:id/update", func(c *gin.Context) {
		tournamentID, err := strconv.Atoi(c.Params.ByName("id"))
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{})
		}

		tournament, err := service.GetTournament(uint(tournamentID))

		if err != nil {
			c.JSON(http.StatusNotFound, gin.H{})
		}

		service.SyncTournament(tournament)

		c.JSON(http.StatusOK, tournament)
	})

	return r
}

func getPort() string {
	value := os.Getenv("GIN_PORT")
	if len(value) == 0 {
		return ":80"
	}
	return value

}

func main() {
	r := setupRouter()
	// Listen and Server in 0.0.0.0:8080
	port := getPort()
	r.Run(port)
}
