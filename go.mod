module gitlab.com/agirdler/inhouse

go 1.13

require (
	github.com/beltsmith/golio v0.0.0-20191229051744-723e7f9da083
	github.com/gin-contrib/cors v1.3.0
	github.com/gin-gonic/gin v1.5.0
	github.com/jinzhu/gorm v1.9.11
	github.com/sirupsen/logrus v1.4.2
)
