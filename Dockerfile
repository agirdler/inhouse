FROM golang:1.13

# now copy your app to the proper build path
RUN mkdir -p $GOPATH/src/app
ADD . $GOPATH/src/app

# should be able to build now
WORKDIR $GOPATH/src/app
RUN go build -o inhouse .
CMD ["/go/src/app/inhouse"]
